#version 460

layout(location = 0) out vec3 fragColor;

const vec2 positions[3] = vec2[](
    vec2(-0.43301, 0.25),
    vec2(0.43301, 0.25),
    vec2(0, -0.5)
);

const vec3 colors[3] = vec3[](
    vec3(1, 0, 0),
    vec3(0, 1, 0),
    vec3(0, 0, 1)
);

void main()
{
    gl_Position = vec4(positions[gl_VertexIndex], 0, 1);
    fragColor = colors[gl_VertexIndex];
}

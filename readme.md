# Multi Attachment Demo

A demostration of using a mixture of floating point and unsigned integer color attachments in Vulkan. Used to compare against LMOD's render pipeline in RenderDoc to find problems.

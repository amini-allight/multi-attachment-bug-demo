#version 460

layout(location = 0) in vec3 fragColor;

layout(location = 0) out vec4 primaryColor;
layout(location = 1) out uvec4 secondaryColor;

void main()
{
    primaryColor.rgb = fragColor;
    primaryColor.a = 1;
    secondaryColor = uvec4(16, 0, 0, 1);
}

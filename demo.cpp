#define VMA_IMPLEMENTATION
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <vector>
#include <limits>

#define CHECK_VK_RESULT(_expr) \
result = _expr; \
if (result != VK_SUCCESS) \
{ \
    cerr << "Error executing " << #_expr << ": " << result << endl; \
}

#define GET_EXTENSION_FUNCTION(_id) reinterpret_cast<PFN_##_id>(vkGetInstanceProcAddr(instance, #_id))

using namespace std;

static string getFile(const string& path)
{
    ifstream file(path);

    return string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
}

struct VmaImage
{
    VkImage image;
    VmaAllocation allocation;
};

struct SwapchainElement
{
    VkCommandBuffer commandBuffer;
    VmaImage multisamplePrimaryImage;
    VmaImage multisampleDepthImage;
    VmaImage multisampleSecondaryImage;
    VkImage outputImage;
    VmaImage primaryImage;
    VmaImage depthImage;
    VmaImage secondaryImage;
    VkImageView multisamplePrimaryImageView;
    VkImageView multisampleDepthImageView;
    VkImageView multisampleSecondaryImageView;
    VkImageView primaryImageView;
    VkImageView depthImageView;
    VkImageView secondaryImageView;
    VkFramebuffer framebuffer;
    VkSemaphore startSemaphore;
    VkSemaphore endSemaphore;
    VkFence fence;
    VkFence lastFence;
};

static VkBool32 onError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "Vulkan ";

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->pMessage << endl;

    return false;
}

static int maxMipLevels(int width, int height)
{
    return floor(log2(max(width, height))) + 1;
}

int main(int argc, char** argv)
{
    VkResult result;

    const char* const programName = "Bug Demo";
    const int width = 1600;
    const int height = 900;
    const VkFormat primaryFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
    const VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;
    const VkFormat secondaryFormat = VK_FORMAT_R8G8B8A8_UINT;
    const VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_4_BIT;
    SDL_Window* window;
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkSurfaceKHR surface;
    VkPhysicalDevice physDevice;
    uint32_t queueFamilyIndex = 0;
    VkDevice device;
    VkQueue queue;
    VmaAllocator allocator;
    VkFormat outputFormat;
    VkSwapchainKHR swapchain;
    VkCommandPool commandPool;
    VkRenderPass renderPass;
    VkShaderModule vertexShader;
    VkShaderModule fragmentShader;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    vector<SwapchainElement> elements;
    uint32_t currentFrame = 0;
    uint32_t imageIndex = 0;

    SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow(programName, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);

    {
        uint32_t extensionNameCount;
        bool ok = SDL_Vulkan_GetInstanceExtensions(window, &extensionNameCount, nullptr);

        if (!ok)
        {
            cerr << "Failed to query Vulkan instance extensions: " << SDL_GetError() << endl;
        }

        vector<const char*> extensionNames(extensionNameCount);
        ok = SDL_Vulkan_GetInstanceExtensions(window, &extensionNameCount, extensionNames.data());

        if (!ok)
        {
            cerr << "Failed to query Vulkan instance extensions: " << SDL_GetError() << endl;
        }

        extensionNames.push_back("VK_EXT_debug_utils");
        vector<const char*> layerNames = { "VK_LAYER_KHRONOS_validation" };

        VkApplicationInfo applicationInfo{};
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = programName;
        applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationInfo.pEngineName = programName;
        applicationInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0);
        applicationInfo.apiVersion = VK_API_VERSION_1_3;

        VkInstanceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &applicationInfo;
        createInfo.enabledExtensionCount = extensionNames.size();
        createInfo.ppEnabledExtensionNames = extensionNames.data();
        createInfo.enabledLayerCount = layerNames.size();
        createInfo.ppEnabledLayerNames = layerNames.data();

        CHECK_VK_RESULT(vkCreateInstance(&createInfo, nullptr, &instance));
    }

    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = onError;

        CHECK_VK_RESULT(GET_EXTENSION_FUNCTION(vkCreateDebugUtilsMessengerEXT)(instance, &createInfo, nullptr, &debugMessenger));
    }

    {
        bool ok = SDL_Vulkan_CreateSurface(window, instance, &surface);

        if (!ok)
        {
            cerr << "Failed to create Vulkan surface: " << SDL_GetError() << endl;
        }
    }

    uint32_t physDeviceCount;
    vkEnumeratePhysicalDevices(instance, &physDeviceCount, nullptr);

    vector<VkPhysicalDevice> physDevices(physDeviceCount);
    vkEnumeratePhysicalDevices(instance, &physDeviceCount, physDevices.data());

    uint32_t bestScore = 0;

    for (VkPhysicalDevice device : physDevices)
    {
        VkPhysicalDeviceProperties properties{};
        vkGetPhysicalDeviceProperties(device, &properties);

        uint32_t score;

        switch (properties.deviceType)
        {
        default :
            continue;
        case VK_PHYSICAL_DEVICE_TYPE_OTHER :
            score = 1;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU :
            score = 4;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU :
            score = 5;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU :
            score = 3;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_CPU :
            score = 2;
            break;
        }

        if (score > bestScore)
        {
            physDevice = device;
            bestScore = score;
        }
    }

    {
        vector<const char*> extensionNames = { "VK_KHR_swapchain" };
        vector<const char*> layerNames = { "VK_LAYER_KHRONOS_validation" };

        uint32_t queueFamilyCount;
        vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, nullptr);

        vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, queueFamilies.data());

        for (uint32_t i = 0; i < queueFamilyCount; i++)
        {
            VkBool32 present = false;

            CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(physDevice, i, surface, &present));

            if (present && (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
            {
                queueFamilyIndex = i;
                break;
            }
        }
        
        float priority = 1;

        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &priority;

        VkPhysicalDeviceMultiviewFeatures multiviewFeatures{};
        multiviewFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
        multiviewFeatures.multiview = true;
        multiviewFeatures.multiviewGeometryShader = true;

        VkPhysicalDeviceFeatures2 features{};
        features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
        features.pNext = &multiviewFeatures;
        features.features.wideLines = true;
        features.features.samplerAnisotropy = true;
        features.features.fillModeNonSolid = true;
        features.features.geometryShader = true;
        features.features.fragmentStoresAndAtomics = true;
        features.features.independentBlend = true;

        VkDeviceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.pNext = &features;
        createInfo.queueCreateInfoCount = 1;
        createInfo.pQueueCreateInfos = &queueCreateInfo;
        createInfo.enabledExtensionCount = extensionNames.size();
        createInfo.ppEnabledExtensionNames = extensionNames.data();
        createInfo.enabledLayerCount = layerNames.size();
        createInfo.ppEnabledLayerNames = layerNames.data();

        CHECK_VK_RESULT(vkCreateDevice(physDevice, &createInfo, nullptr, &device));

        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
    }

    {
        VmaAllocatorCreateInfo createInfo{};
        createInfo.physicalDevice = physDevice;
        createInfo.device = device;
        createInfo.instance = instance;

        CHECK_VK_RESULT(vmaCreateAllocator(&createInfo, &allocator));
    }

    {
        VkSurfaceCapabilitiesKHR capabilities;
        CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physDevice, surface, &capabilities));

        uint32_t formatCount;
        CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, nullptr));

        vector<VkSurfaceFormatKHR> formats(formatCount);
        CHECK_VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, formats.data()));

        VkSurfaceFormatKHR chosenFormat = formats.front();

        for (const VkSurfaceFormatKHR& format : formats)
        {
            if (format.format == VK_FORMAT_B8G8R8A8_UNORM)
            {
                chosenFormat = format;
                break;
            }
        }

        outputFormat = chosenFormat.format;

        uint32_t minImageCount = capabilities.minImageCount + 1 < capabilities.maxImageCount
            ? capabilities.minImageCount + 1
            : capabilities.minImageCount;

        VkSwapchainCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface;
        createInfo.minImageCount = minImageCount;
        createInfo.imageFormat = chosenFormat.format;
        createInfo.imageColorSpace = chosenFormat.colorSpace;
        createInfo.imageExtent = { width, height };
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.preTransform = capabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
        createInfo.clipped = true;

        CHECK_VK_RESULT(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain));
    }

    {
        VkCommandPoolCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        createInfo.queueFamilyIndex = queueFamilyIndex;
        createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

        CHECK_VK_RESULT(vkCreateCommandPool(device, &createInfo, nullptr, &commandPool));
    }

    {
        VkAttachmentDescription2 multisamplePrimaryAttachment{};
        multisamplePrimaryAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        multisamplePrimaryAttachment.format = primaryFormat;
        multisamplePrimaryAttachment.samples = sampleCount;
        multisamplePrimaryAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        multisamplePrimaryAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisamplePrimaryAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        multisamplePrimaryAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisamplePrimaryAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        multisamplePrimaryAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 multisamplePrimaryAttachmentRef{};
        multisamplePrimaryAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        multisamplePrimaryAttachmentRef.attachment = 0;
        multisamplePrimaryAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription2 multisampleDepthAttachment{};
        multisampleDepthAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        multisampleDepthAttachment.format = depthFormat;
        multisampleDepthAttachment.samples = sampleCount;
        multisampleDepthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        multisampleDepthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisampleDepthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        multisampleDepthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisampleDepthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        multisampleDepthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 multisampleDepthAttachmentRef{};
        multisampleDepthAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        multisampleDepthAttachmentRef.attachment = 1;
        multisampleDepthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription2 multisampleSecondaryAttachment{};
        multisampleSecondaryAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        multisampleSecondaryAttachment.format = secondaryFormat;
        multisampleSecondaryAttachment.samples = sampleCount;
        multisampleSecondaryAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        multisampleSecondaryAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisampleSecondaryAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        multisampleSecondaryAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        multisampleSecondaryAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        multisampleSecondaryAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 multisampleSecondaryAttachmentRef{};
        multisampleSecondaryAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        multisampleSecondaryAttachmentRef.attachment = 2;
        multisampleSecondaryAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription2 primaryAttachment{};
        primaryAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        primaryAttachment.format = primaryFormat;
        primaryAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        primaryAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        primaryAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        primaryAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        primaryAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        primaryAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        primaryAttachment.finalLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

        VkAttachmentReference2 primaryAttachmentRef{};
        primaryAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        primaryAttachmentRef.attachment = 3;
        primaryAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription2 depthAttachment{};
        depthAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        depthAttachment.format = depthFormat;
        depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 depthAttachmentRef{};
        depthAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        depthAttachmentRef.attachment = 4;
        depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentDescription2 secondaryAttachment{};
        secondaryAttachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
        secondaryAttachment.format = secondaryFormat;
        secondaryAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        secondaryAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        secondaryAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        secondaryAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        secondaryAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        secondaryAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        secondaryAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 secondaryAttachmentRef{};
        secondaryAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
        secondaryAttachmentRef.attachment = 5;
        secondaryAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference2 colorAttachments[] = {
            multisamplePrimaryAttachmentRef,
            multisampleSecondaryAttachmentRef
        };

        VkAttachmentReference2 resolveAttachments[] = {
            primaryAttachmentRef,
            secondaryAttachmentRef
        };

        VkSubpassDescriptionDepthStencilResolve depthStencilResolve{};
        depthStencilResolve.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE;
        depthStencilResolve.depthResolveMode = VK_RESOLVE_MODE_MAX_BIT;
        depthStencilResolve.stencilResolveMode = VK_RESOLVE_MODE_SAMPLE_ZERO_BIT;
        depthStencilResolve.pDepthStencilResolveAttachment = &depthAttachmentRef;

        VkSubpassDescription2 subpass{};
        subpass.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2;
        subpass.pNext = &depthStencilResolve;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = sizeof(colorAttachments) / sizeof(VkAttachmentReference2);
        subpass.pColorAttachments = colorAttachments;
        subpass.pDepthStencilAttachment = &multisampleDepthAttachmentRef;
        subpass.pResolveAttachments = resolveAttachments;

        VkSubpassDependency2 previousToSubpass{};
        previousToSubpass.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
        previousToSubpass.srcSubpass = VK_SUBPASS_EXTERNAL;
        previousToSubpass.dstSubpass = 0;
        previousToSubpass.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        previousToSubpass.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
        previousToSubpass.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        previousToSubpass.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        VkSubpassDependency2 subpassToNext{};
        subpassToNext.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
        subpassToNext.srcSubpass = 0;
        subpassToNext.dstSubpass = VK_SUBPASS_EXTERNAL;
        subpassToNext.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassToNext.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpassToNext.dstStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;
        subpassToNext.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

        VkAttachmentDescription2 attachments[] = {
            multisamplePrimaryAttachment,
            multisampleDepthAttachment,
            multisampleSecondaryAttachment,
            primaryAttachment,
            depthAttachment,
            secondaryAttachment
        };

        VkSubpassDependency2 dependencies[] = {
            previousToSubpass,
            subpassToNext
        };

        VkRenderPassCreateInfo2 createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
        createInfo.flags = 0;
        createInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
        createInfo.pAttachments = attachments;
        createInfo.subpassCount = 1;
        createInfo.pSubpasses = &subpass;
        createInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
        createInfo.pDependencies = dependencies;

        CHECK_VK_RESULT(vkCreateRenderPass2(device, &createInfo, nullptr, &renderPass));
    }

    {
        string code = getFile("vertex.spv");

        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        CHECK_VK_RESULT(vkCreateShaderModule(device, &createInfo, nullptr, &vertexShader));
    }

    {
        string code = getFile("fragment.spv");

        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        CHECK_VK_RESULT(vkCreateShaderModule(device, &createInfo, nullptr, &fragmentShader));
    }

    {
        VkPipelineLayoutCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

        CHECK_VK_RESULT(vkCreatePipelineLayout(device, &createInfo, nullptr, &pipelineLayout));
    }

    {
        VkPipelineVertexInputStateCreateInfo vertexInputState{};
        vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputState.vertexBindingDescriptionCount = 0;
        vertexInputState.pVertexBindingDescriptions = nullptr;
        vertexInputState.vertexAttributeDescriptionCount = 0;
        vertexInputState.pVertexAttributeDescriptions = nullptr;

        VkPipelineInputAssemblyStateCreateInfo inputAssemblyState{};
        inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyState.primitiveRestartEnable = false;

        VkViewport viewport = {
            0, 0,
            1024, 1024,
            0, 1
        };

        VkRect2D scissor = {
            { 0, 0 },
            { 1024, 1024 }
        };

        VkPipelineViewportStateCreateInfo viewportState{};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizationState{};
        rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationState.depthClampEnable = false;
        rasterizationState.rasterizerDiscardEnable = false;
        rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationState.lineWidth = 1;
        rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationState.depthBiasEnable = false;
        rasterizationState.depthBiasConstantFactor = 0;
        rasterizationState.depthBiasClamp = 0;
        rasterizationState.depthBiasSlopeFactor = 0;

        VkPipelineMultisampleStateCreateInfo multisampleState{};
        multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleState.rasterizationSamples = sampleCount;

        VkPipelineDepthStencilStateCreateInfo depthStencilState{};
        depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilState.depthTestEnable = true;
        depthStencilState.depthWriteEnable = true;
        depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencilState.depthBoundsTestEnable = false;
        depthStencilState.minDepthBounds = 0;
        depthStencilState.maxDepthBounds = 1;
        depthStencilState.stencilTestEnable = false;

        VkPipelineColorBlendAttachmentState primaryColorBlendAttachment{};
        primaryColorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        primaryColorBlendAttachment.blendEnable = true;
        primaryColorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        primaryColorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        primaryColorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
        primaryColorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        primaryColorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        primaryColorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

        VkPipelineColorBlendAttachmentState secondaryColorBlendAttachment{};
        secondaryColorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        VkPipelineColorBlendAttachmentState colorBlendAttachments[] = {
            primaryColorBlendAttachment,
            secondaryColorBlendAttachment
        };

        VkPipelineColorBlendStateCreateInfo colorBlendState{};
        colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendState.logicOpEnable = false;
        colorBlendState.logicOp = VK_LOGIC_OP_COPY;
        colorBlendState.attachmentCount = sizeof(colorBlendAttachments) / sizeof(VkPipelineColorBlendAttachmentState);
        colorBlendState.pAttachments = colorBlendAttachments;
        colorBlendState.blendConstants[0] = 0;
        colorBlendState.blendConstants[1] = 0;
        colorBlendState.blendConstants[2] = 0;
        colorBlendState.blendConstants[3] = 0;

        VkDynamicState dynamicStates[] = {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_SCISSOR
        };

        VkPipelineDynamicStateCreateInfo dynamicState{};
        dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicState.dynamicStateCount = 2;
        dynamicState.pDynamicStates = dynamicStates;

        VkPipelineShaderStageCreateInfo vertexShaderStage{};
        vertexShaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertexShaderStage.module = vertexShader;
        vertexShaderStage.pName = "main";

        VkPipelineShaderStageCreateInfo fragmentShaderStage{};
        fragmentShaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragmentShaderStage.module = fragmentShader;
        fragmentShaderStage.pName = "main";

        VkPipelineShaderStageCreateInfo stages[] = {
            vertexShaderStage,
            fragmentShaderStage
        };

        VkGraphicsPipelineCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.layout = pipelineLayout;
        createInfo.stageCount = sizeof(stages) / sizeof(VkPipelineShaderStageCreateInfo);
        createInfo.pStages = stages;
        createInfo.pVertexInputState = &vertexInputState;
        createInfo.pInputAssemblyState = &inputAssemblyState;
        createInfo.pViewportState = &viewportState;
        createInfo.pRasterizationState = &rasterizationState;
        createInfo.pMultisampleState = &multisampleState;
        createInfo.pDepthStencilState = &depthStencilState;
        createInfo.pColorBlendState = &colorBlendState;
        createInfo.pDynamicState = &dynamicState;
        createInfo.renderPass = renderPass;
        createInfo.subpass = 0;

        CHECK_VK_RESULT(vkCreateGraphicsPipelines(device, nullptr, 1, &createInfo, nullptr, &pipeline));
    }

    {
        uint32_t imageCount;
        CHECK_VK_RESULT(vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr));

        vector<VkImage> images(imageCount);
        CHECK_VK_RESULT(vkGetSwapchainImagesKHR(device, swapchain, &imageCount, images.data()));

        for (VkImage image : images)
        {
            SwapchainElement element;
            {
                VkCommandBufferAllocateInfo allocInfo{};
                allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                allocInfo.commandPool = commandPool;
                allocInfo.commandBufferCount = 1;
                allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

                CHECK_VK_RESULT(vkAllocateCommandBuffers(device, &allocInfo, &element.commandBuffer));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = sampleCount;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = primaryFormat;
                createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.multisamplePrimaryImage.image, &element.multisamplePrimaryImage.allocation, nullptr));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = sampleCount;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = depthFormat;
                createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.multisampleDepthImage.image, &element.multisampleDepthImage.allocation, nullptr));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = sampleCount;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = secondaryFormat;
                createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.multisampleSecondaryImage.image, &element.multisampleSecondaryImage.allocation, nullptr));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = primaryFormat;
                createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.primaryImage.image, &element.primaryImage.allocation, nullptr));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = depthFormat;
                createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.depthImage.image, &element.depthImage.allocation, nullptr));
            }

            {
                VkImageCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
                createInfo.imageType = VK_IMAGE_TYPE_2D;
                createInfo.extent = { width, height, 1 };
                createInfo.mipLevels = 1;
                createInfo.arrayLayers = 1;
                createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
                createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
                createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                createInfo.format = secondaryFormat;
                createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

                VmaAllocationCreateInfo allocateInfo{};
                allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                CHECK_VK_RESULT(vmaCreateImage(allocator, &createInfo, &allocateInfo, &element.secondaryImage.image, &element.secondaryImage.allocation, nullptr));
            }

            element.outputImage = image;

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.multisamplePrimaryImage.image;
                createInfo.format = primaryFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.multisamplePrimaryImageView));
            }

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.multisampleDepthImage.image;
                createInfo.format = depthFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.multisampleDepthImageView));
            }

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.multisampleSecondaryImage.image;
                createInfo.format = secondaryFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.multisampleSecondaryImageView));
            }

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.primaryImage.image;
                createInfo.format = primaryFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.primaryImageView));
            }

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.depthImage.image;
                createInfo.format = depthFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.depthImageView));
            }

            {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                createInfo.image = element.secondaryImage.image;
                createInfo.format = secondaryFormat;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

                CHECK_VK_RESULT(vkCreateImageView(device, &createInfo, nullptr, &element.secondaryImageView));
            }

            {
                VkImageView attachments[] = { element.multisamplePrimaryImageView, element.multisampleDepthImageView, element.multisampleSecondaryImageView, element.primaryImageView, element.depthImageView, element.secondaryImageView };

                VkFramebufferCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                createInfo.renderPass = renderPass;
                createInfo.attachmentCount = sizeof(attachments) / sizeof(VkImageView);
                createInfo.pAttachments = attachments;
                createInfo.width = width;
                createInfo.height = height;
                createInfo.layers = 1;

                CHECK_VK_RESULT(vkCreateFramebuffer(device, &createInfo, nullptr, &element.framebuffer));
            }

            {
                VkSemaphoreCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

                CHECK_VK_RESULT(vkCreateSemaphore(device, &createInfo, nullptr, &element.startSemaphore));
            }

            {
                VkSemaphoreCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

                CHECK_VK_RESULT(vkCreateSemaphore(device, &createInfo, nullptr, &element.endSemaphore));
            }

            {
                VkFenceCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

                CHECK_VK_RESULT(vkCreateFence(device, &createInfo, nullptr, &element.fence));
            }

            element.lastFence = nullptr;

            elements.push_back(element);
        }
    }

    bool quit = false;
    while (!quit)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            quit |= event.type == SDL_QUIT;
        }

        SwapchainElement& currentElement = elements.at(currentFrame);

        CHECK_VK_RESULT(vkWaitForFences(device, 1, &currentElement.fence, true, numeric_limits<uint64_t>::max()));
        result = vkAcquireNextImageKHR(device, swapchain, numeric_limits<uint64_t>::max(), currentElement.startSemaphore, nullptr, &imageIndex);

        if (result != VK_SUBOPTIMAL_KHR)
        {
            CHECK_VK_RESULT(result);
        }

        SwapchainElement& element = elements.at(imageIndex);

        if (element.lastFence)
        {
            CHECK_VK_RESULT(vkWaitForFences(device, 1, &element.lastFence, true, numeric_limits<uint64_t>::max()));
        }

        element.lastFence = currentElement.fence;

        CHECK_VK_RESULT(vkResetFences(device, 1, &currentElement.fence));

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        CHECK_VK_RESULT(vkBeginCommandBuffer(element.commandBuffer, &beginInfo));

        {
            VkClearValue primaryClearValue = {{
                0.0f,
                0.0f,
                0.0f,
                1.0f
            }};

            VkClearValue depthClearValue{};
            depthClearValue.depthStencil.depth = 1.0f;

            VkClearValue secondaryClearValue = {{
                2.0f,
                0.0f,
                0.0f,
                1.0f
            }};

            VkClearValue clearValues[] = {
                primaryClearValue,
                depthClearValue,
                secondaryClearValue,
                primaryClearValue,
                depthClearValue,
                secondaryClearValue
            };

            VkRenderPassBeginInfo beginInfo{};
            beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            beginInfo.renderPass = renderPass;
            beginInfo.framebuffer = element.framebuffer;
            beginInfo.renderArea = {
                { 0, 0 },
                { width, height }
            };
            beginInfo.clearValueCount = sizeof(clearValues) / sizeof(VkClearValue);
            beginInfo.pClearValues = clearValues;

            vkCmdBeginRenderPass(element.commandBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
            vkCmdBindPipeline(element.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
            VkViewport viewport = {
                0, 0,
                width, height,
                0, 1
            };

            vkCmdSetViewport(element.commandBuffer, 0, 1, &viewport);
            VkRect2D scissor = {
                { 0, 0 },
                { width, height }
            };

            vkCmdSetScissor(element.commandBuffer, 0, 1, &scissor);
            vkCmdDraw(element.commandBuffer, 3, 1, 0, 0);
            vkCmdEndRenderPass(element.commandBuffer);

            VkImageMemoryBarrier beforeBarrier{};
            beforeBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            beforeBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            beforeBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            beforeBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            beforeBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            beforeBarrier.image = element.outputImage;
            beforeBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            beforeBarrier.subresourceRange.baseMipLevel = 0;
            beforeBarrier.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
            beforeBarrier.subresourceRange.baseArrayLayer = 0;
            beforeBarrier.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

            vkCmdPipelineBarrier(element.commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &beforeBarrier);

            VkImageBlit region{};
            region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.srcSubresource.mipLevel = 0;
            region.srcSubresource.layerCount = 1;
            region.srcOffsets[0] = { 0, 0, 0 };
            region.srcOffsets[1] = { width, height, 1 };
            region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.dstSubresource.mipLevel = 0;
            region.dstSubresource.layerCount = 1;
            region.dstOffsets[0] = { 0, 0, 0 };
            region.dstOffsets[1] = { width, height, 1 };

            vkCmdBlitImage(element.commandBuffer, element.primaryImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, element.outputImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region, VK_FILTER_NEAREST);

            VkImageMemoryBarrier afterBarrier{};
            afterBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            afterBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            afterBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            afterBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            afterBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            afterBarrier.image = element.outputImage;
            afterBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            afterBarrier.subresourceRange.baseMipLevel = 0;
            afterBarrier.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
            afterBarrier.subresourceRange.baseArrayLayer = 0;
            afterBarrier.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

            vkCmdPipelineBarrier(element.commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &afterBarrier);
        }

        CHECK_VK_RESULT(vkEndCommandBuffer(element.commandBuffer));

        const VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &currentElement.startSemaphore;
        submitInfo.pWaitDstStageMask = &waitStage;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &element.commandBuffer;
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = &currentElement.endSemaphore;

        CHECK_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, currentElement.fence));

        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &currentElement.endSemaphore;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &swapchain;
        presentInfo.pImageIndices = &imageIndex;

        result = vkQueuePresentKHR(queue, &presentInfo);

        if (result != VK_SUBOPTIMAL_KHR)
        {
            CHECK_VK_RESULT(result);
        }

        currentFrame = (currentFrame + 1) % elements.size();
    }

    CHECK_VK_RESULT(vkDeviceWaitIdle(device));

    for (const SwapchainElement& element : elements)
    {
        vkDestroyFence(device, element.fence, nullptr);
        vkDestroySemaphore(device, element.endSemaphore, nullptr);
        vkDestroySemaphore(device, element.startSemaphore, nullptr);

        vkDestroyFramebuffer(device, element.framebuffer, nullptr);

        vkDestroyImageView(device, element.secondaryImageView, nullptr);
        vkDestroyImageView(device, element.depthImageView, nullptr);
        vkDestroyImageView(device, element.primaryImageView, nullptr);
        vkDestroyImageView(device, element.multisampleSecondaryImageView, nullptr);
        vkDestroyImageView(device, element.multisampleDepthImageView, nullptr);
        vkDestroyImageView(device, element.multisamplePrimaryImageView, nullptr);

        vmaDestroyImage(allocator, element.secondaryImage.image, element.secondaryImage.allocation);
        vmaDestroyImage(allocator, element.depthImage.image, element.depthImage.allocation);
        vmaDestroyImage(allocator, element.primaryImage.image, element.primaryImage.allocation);
        vmaDestroyImage(allocator, element.multisampleSecondaryImage.image, element.multisampleSecondaryImage.allocation);
        vmaDestroyImage(allocator, element.multisampleDepthImage.image, element.multisampleDepthImage.allocation);
        vmaDestroyImage(allocator, element.multisamplePrimaryImage.image, element.multisamplePrimaryImage.allocation);

        vkFreeCommandBuffers(device, commandPool, 1, &element.commandBuffer);
    }

    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    vkDestroyShaderModule(device, fragmentShader, nullptr);
    vkDestroyShaderModule(device, vertexShader, nullptr);
    vkDestroyRenderPass(device, renderPass, nullptr);
    vkDestroyCommandPool(device, commandPool, nullptr);
    vkDestroySwapchainKHR(device, swapchain, nullptr);
    vmaDestroyAllocator(allocator);
    vkDestroyDevice(device, nullptr);
    vkDestroySurfaceKHR(instance, surface, nullptr);
    GET_EXTENSION_FUNCTION(vkDestroyDebugUtilsMessengerEXT)(instance, debugMessenger, nullptr);
    vkDestroyInstance(instance, nullptr);

    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}
